# Survey on Model-Based Design Reviews

This project stores the code in R corresponding to the statistical analysis of the survey regarding Model-Based Design rewiews practices and challenges.

The analysis was conducted in 2021 by Romain Pinquié (<romain.pinquie@grenoble-inp.fr>) and Victor Romero (<victor.romero@grenoble-inp.fr>).

## Installation

1. Clone the repository
2. Install R
3. Install R Studio
4. Open the R project "MBD_Review_Survey.Rproj"
5. Run the script "Analysis.R"

## Citation

If you use the raw dataset (DOI), we would appreciate citations:

If you use the R code (DOI), we would appreciate citations:

If you use the paper (DOI), we would appreciate citations:

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
